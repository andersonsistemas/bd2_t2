/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import com.mysql.jdbc.ResultSetMetaData;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.Timer;
/**
 *
 * @author anderson
 */
public class GUI extends javax.swing.JFrame {

   
    public GUI() {
        
        initComponents();
        timer.start();
        
    }
    Timer timer = new Timer(1000, new ActionListener(){
      public void actionPerformed(ActionEvent e){
        carregaComboBoxTabelas();
       timer.stop();
      }
    });
    
    
    
    
private static Connection getConexao() throws SQLException
   	{
  		
                String url = "jdbc:mysql://localhost:3306/bd2_t2?autoReconnect=true&useSSL=false";
   		
   		try
   		{
                Class.forName("com.mysql.jdbc.Driver");    
    		
    	}
  
    	catch (ClassNotFoundException e) 
    	{
    		e.printStackTrace();
    		System.err.println("MySql jdbc driver not found.");
  
    		System.exit(-1);
    	}
    	Connection conn = DriverManager.getConnection(url, "root", "root");
    
    	return conn;	
    }

    public void carregaComboBoxTabelas()  
    {  
            try  
            {    
              
                               
                Connection conn;  
                conn = getConexao();
                //jCBx_tabela.addItem("");  
                DatabaseMetaData db_md = conn.getMetaData();
                ResultSet res = db_md.getTables(null, null, "%", null);
                ResultSetMetaData meta = (ResultSetMetaData) res.getMetaData();
                while(res.next())  
                { 
                   
                    String nomeTabela = res.getString("TABLE_NAME");
                    jCBx_tabela.addItem(nomeTabela);
                   
                }
                jCBx_tabela.setSelectedIndex(0);
                jCBx_tabela.setMaximumRowCount(10);
                jSpn_Concorrent_Transact.setValue(10);
                jSpn_Total_Transact.setValue(1000);
                
                
                
                res.close();  
                conn.close();
                
            }  
            catch(Exception e)  
            {  
                JOptionPane.showMessageDialog(null,   
                        "Ocorreu erro ao carregar a Combo Box", "Erro",  
                        JOptionPane.ERROR_MESSAGE);  
         } 
          
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jbtnStart = new javax.swing.JButton();
        jCBx_Isolamento_level = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jCbx_verbose = new javax.swing.JCheckBox();
        jCBx_tabela = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        jSpn_Total_Transact = new javax.swing.JSpinner();
        jLabel3 = new javax.swing.JLabel();
        jSpn_Concorrent_Transact = new javax.swing.JSpinner();
        jLabel4 = new javax.swing.JLabel();
        jCbx_estrategia = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jbtnStart.setText("Start Test");
        jbtnStart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbtnStartActionPerformed(evt);
            }
        });

        jCBx_Isolamento_level.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "READ_UNCOMMITTED", "READ_COMMITTED", "REPEATABLE_READ", "SERIALIZABLE" }));
        jCBx_Isolamento_level.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCBx_Isolamento_levelActionPerformed(evt);
            }
        });

        jLabel1.setText("Nível de isolamento");

        jCbx_verbose.setText("Verbose");

        jLabel2.setText("Tabela");

        jLabel3.setText("Total de Transações");

        jLabel4.setText("Máximo de Transações Concorrentes");

        jCbx_estrategia.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2" }));

        jLabel5.setText("Estratégia");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jCBx_tabela, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(40, 40, 40)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSpn_Concorrent_Transact, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 248, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jCbx_estrategia, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(161, 161, 161))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jCBx_Isolamento_level, javax.swing.GroupLayout.PREFERRED_SIZE, 239, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(35, 35, 35)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jSpn_Total_Transact, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jbtnStart, javax.swing.GroupLayout.PREFERRED_SIZE, 480, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jCbx_verbose))
                        .addContainerGap(65, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel3)
                        .addGap(2, 2, 2)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCBx_Isolamento_level, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSpn_Total_Transact, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jCbx_verbose)
                .addGap(5, 5, 5)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCBx_tabela, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSpn_Concorrent_Transact, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCbx_estrategia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(jbtnStart, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jCBx_Isolamento_levelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCBx_Isolamento_levelActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCBx_Isolamento_levelActionPerformed

    private void jbtnStartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbtnStartActionPerformed
        
        ConcurrentTransactions ct = new ConcurrentTransactions();
        ct.setIsStr(jCBx_Isolamento_level.getSelectedItem().toString());
        ct.setMaxConcurrent((int) jSpn_Concorrent_Transact.getValue());
        ct.setNumEmployees((int) jSpn_Total_Transact.getValue());
        ct.setTable((String)jCBx_tabela.getSelectedItem());
        ct.setVerbose(jCbx_verbose.isSelected());
        ct.setWorkload(Integer.parseInt(jCbx_estrategia.getSelectedItem().toString()));
        try {
            ct.Executar();
        } catch (SQLException ex) {
            Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            ct.populateDatabase();
        } catch (SQLException ex) {
            Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        
                
                
    }//GEN-LAST:event_jbtnStartActionPerformed

   

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> jCBx_Isolamento_level;
    private javax.swing.JComboBox<String> jCBx_tabela;
    private javax.swing.JComboBox<String> jCbx_estrategia;
    private javax.swing.JCheckBox jCbx_verbose;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JSpinner jSpn_Concorrent_Transact;
    private javax.swing.JSpinner jSpn_Total_Transact;
    private javax.swing.JButton jbtnStart;
    // End of variables declaration//GEN-END:variables
}
